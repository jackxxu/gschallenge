class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :state
      t.string :first_name
      t.string :last_name
      t.string :title
      t.string :phone
      t.string :loan_number
      t.integer :loan_amount
      t.float :interest_rate
      t.string :maturity_date

      t.timestamps null: false
    end
  end
end
