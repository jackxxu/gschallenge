require 'csv'

class AddLoanFixtures < ActiveRecord::Migration

  def change
    file_path = File.expand_path(File.join(File.dirname(__FILE__), "../loans.csv"))
    csv_text = File.read(file_path)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      Loan.create!(row.to_hash)
    end
  end
end
